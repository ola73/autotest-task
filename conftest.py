#
# Operating System: Debian GNU/Linux 11 (bullseye)
# Kernel: Linux 5.14.0-0.bpo.2-amd64
#
# Python Version: 3.9
#
# Pytest Version: 6.2.5
#
# Selenium Version: 4.1.0
#
# Driver: chromedriver_linux64
# Driver Version 97.0.4692.71
# Web Browser Version: 97.0.4692.99 (Official Build) (64-bit)
#

import pytest
from selenium import webdriver


@pytest.fixture(scope='session')
def browser():
    driver_path = './src/chromedriver_linux64/chromedriver'
    driver = webdriver.Chrome(executable_path=driver_path)
    # driver.implicitly_wait(5)
    yield driver
    driver.quit()
