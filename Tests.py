import time

import allure
from selenium.webdriver.common.keys import Keys
from allure_commons.types import AttachmentType

from YandexPages import UserSearch, UserImage
from YandexPages import YandexLocators as YL


def __open_user_search(browser):
    page = UserSearch(browser)
    page.go_to_page()
    return page


def __open_user_image(browser):
    page = UserImage(browser)
    page.go_to_page()
    page.go_to_images_page()
    return page


def __action_fixation(browser, test_number):
    with allure.step(f'Do screenshot of the {test_number} test'):
        allure.attach(
            browser.do_screenshot(),
            name=f'Screenshot_{test_number}_test',
            attachment_type=AttachmentType.PNG
        )


@allure.feature('Open main page')
@allure.story('Проверка наличия поисковой строки')
@allure.severity('blocker')
def test_is_yandex_search(browser):
    """
        Checks if search field exists.
    """

    page = __open_user_search(browser)
    __action_fixation(page, 1)
    assert page.search_element_count(YL.LOCATOR_SEARCH_FIELD)


@allure.feature('Open main page')
@allure.story('Проверка наличия подсказок поисковой строки')
@allure.severity('normal')
def test_is_suggest(browser):
    """
        Checks if a 'suggest' exists.
    """

    page = __open_user_search(browser)
    page.word_search('Тензор')
    time.sleep(1)
    __action_fixation(page, 2)
    assert page.search_element_count(YL.LOCATOR_SUGGEST)


@allure.feature('Open main page')
@allure.story('Проверка наличия url адресов по поиску "Тензор"')
@allure.severity('blocker')
def test_url_presence(browser):
    """
        Checks if the url 'tensor.ru' exists in the search result.
    """

    page = __open_user_search(browser)
    page.word_search('Тензор').send_keys(Keys.ENTER)
    __action_fixation(page, 3)
    assert page.search_url_count('tensor.ru') >= 1


@allure.feature('Open image page')
@allure.story('Проверка перехода на другой url адрес')
@allure.severity('critical')
def test_current_url(browser):
    """
        Checks that the current address is https://yandex.ru/images/
    """
    page = __open_user_image(browser)
    __action_fixation(page, 4)
    assert page.get_url().startswith("https://yandex.ru/images/")


@allure.feature('Open image page')
@allure.story('Проверка перехода по первой категории')
@allure.severity('normal')
def test_top_category(browser):
    """
        Checks if the search contains a top category name.
    """

    page = __open_user_image(browser)
    name_category = page.collect_top_category_name()
    __action_fixation(page, 5)
    page.click_on_element(YL.LOCATOR_TOP_CATEGORY)
    search_results = page.get_info_search_results()
    __action_fixation(page, 6)
    assert name_category == search_results


@allure.feature('Open image page')
@allure.story('Проверка открытия картинки')
@allure.severity('minor')
def test_is_open_image(browser):
    """
        Checks that the image is open.
    """

    page = __open_user_image(browser)
    page.click_on_element(YL.LOCATOR_TOP_CATEGORY)
    __action_fixation(page, 7)
    page.click_on_element(YL.LOCATOR_IMAGES)
    __action_fixation(page, 8)
    assert page.is_opened_image()


@allure.feature('Open image page')
@allure.story('Проверка переключения между картинками')
@allure.severity('normal')
def test_image_switching(browser):
    """
        Checks that after switching, the image is the same.
    """

    page = __open_user_image(browser)
    page.click_on_element(YL.LOCATOR_TOP_CATEGORY)
    __action_fixation(page, 9)
    page.click_on_element(YL.LOCATOR_IMAGES)
    __action_fixation(page, 10)

    image_url = page.get_url()
    page.switching_image(YL.LOCATOR_NEXT_BUTTON)
    __action_fixation(page, 11)
    page.switching_image(YL.LOCATOR_PREVIOUS_BUTTON)
    __action_fixation(page, 12)

    assert page.get_url() == image_url
