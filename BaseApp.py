from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, browser: WebDriver) -> None:
        self.browser = browser
        self.url = 'https://yandex.ru/'

    def go_to_page(self):
        return self.browser.get(self.url)

    def get_url(self) -> str:
        return self.browser.current_url

    def do_screenshot(self):
        return self.browser.get_screenshot_as_png()

    def change_tab(self, tab: int):
        tabs = self.browser.window_handles
        return self.browser.switch_to.window(tabs[tab])

    def find_element(self, locator: tuple, timeout: int = 6):
        try:
            return WebDriverWait(self.browser, timeout).until(
                EC.presence_of_element_located(locator),
                message=f"Locator <{locator}>: not found element",
            )
        except TimeoutException:
            return 0

    def find_elements(self, locator: tuple, timeout: int = 10):
        try:
            return WebDriverWait(self.browser, timeout).until(
                EC.presence_of_all_elements_located(locator),
                message=f"Locator <{locator}>: not found elements"
            )
        except TimeoutException:
            return []

    def wait_change_url(self, url: str, timeout: int = 10):
        return WebDriverWait(self.browser, timeout).until(
            lambda browser: browser.current_url != url
        )
