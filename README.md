# Установка Allure и других зависимостей
![allure-logo](https://miro.medium.com/max/871/1*kG70iNbY3-pG0jd_3Mdaxg.png)


## Устновка зависимостей

* Для установки зависимостей необходимо скачать модули:

```bash
$ pip3 install pytest==6.2.5
    
$ pip3 install selenium==4.1.0
    
$ pip3 install allure-pytest==2.9.45
    
$ pip3 install allure-python-commons==2.9.45
```


Или можно установить зависимости, запустив следующую команду:

```bash
$ pip3 install -r requirements.txt
```

## Установка Allure и запуск тестов

### Скачивание Allure

Для устновки необходимо:
1. Загрузить версию 2.17.2 в виде zip-архива с [Maven Central][1]
2. Распаковать архив в каталог allure-commandline.
3. Перейти в каталог bin.
4. Использовать `allure` для платформ Unix.

* Для проверки скачивания можно запустить команду<br/>
```bash
$ ~/allure-2.17.2/bin/allure --version
```

### Запуск тестов и просмотр отчетов

Перейдите в директорию tensor_test_task.

* Для запуска тестов выполнить команду:

```bash
$ python3 -m pytest Tests.py --alluredir results
```

* Для просмотра отчетов по тестам запустить:

```bash
$ ~/allure-2.17.2/bin/allure serve results
```

[1]: (https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.17.2/)