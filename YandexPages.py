from enum import Enum
from urllib.parse import unquote

from selenium.webdriver.common.by import By

import BaseApp


class YandexLocators(Enum):
    LOCATOR_SEARCH_FIELD = (By.ID, 'text')
    LOCATOR_SUGGEST = (By.CLASS_NAME, 'mini-suggest__item_type_fulltext')
    LOCATOR_URL = (By.CLASS_NAME, 'Link_theme_outer')

    LOCATOR_IMAGES_PAGE = (By.XPATH, '/html/body/div[1]/div[2]/div[3]/div/div[1]/nav/div/ul/li[3]/a/div[1]')
    LOCATOR_TOP_CATEGORY = (By.CLASS_NAME, 'PopularRequestList-Item_pos_0')
    LOCATOR_IMAGES = (By.CLASS_NAME, 'serp-item__thumb')
    LOCATOR_OPEN_IMAGE = (By.CLASS_NAME, 'MediaViewer-LayoutMain')
    LOCATOR_NEXT_BUTTON = (By.CLASS_NAME, 'MediaViewer_theme_fiji-ButtonNext')
    LOCATOR_PREVIOUS_BUTTON = (By.CLASS_NAME, 'MediaViewer_theme_fiji-ButtonPrev')


class UserSearch(BaseApp.BasePage):
    def word_search(self, word: str):
        search_field = self.find_element(YandexLocators.LOCATOR_SEARCH_FIELD.value)
        search_field.clear()
        search_field.send_keys(word)
        return search_field

    def search_element_count(self, locator: YandexLocators) -> int:
        return len(self.find_elements(locator.value, 3))

    def search_url_count(self, url: str) -> int:
        object_urls = self.find_elements(YandexLocators.LOCATOR_URL.value)[:5]
        urls = [i.text for i in object_urls]
        return urls.count(url)


class UserImage(BaseApp.BasePage):
    def go_to_images_page(self):
        self.click_on_element(YandexLocators.LOCATOR_IMAGES_PAGE)
        return self.change_tab(-1)

    def click_on_element(self, locator: YandexLocators):
        image_button = self.find_element(locator.value)
        return image_button.click()

    def collect_top_category_name(self) -> str:
        top_category = self.find_element(YandexLocators.LOCATOR_TOP_CATEGORY.value)
        return top_category.text

    @staticmethod
    def __decode_url(url: str) -> str:
        start_find = url.find('&text') + 6
        finish_find = url.find('&nl')

        decoded_cyrillic = unquote(url[start_find:finish_find], 'utf8')
        return decoded_cyrillic

    def get_info_search_results(self) -> str:
        url = self.get_url()
        result = UserImage.__decode_url(url)
        return result

    def is_opened_image(self) -> bool:
        image = self.find_element(YandexLocators.LOCATOR_OPEN_IMAGE.value)
        return image.is_displayed()

    def switching_image(self, locator):
        url = self.get_url()
        self.click_on_element(locator)
        return self.wait_change_url(url)
